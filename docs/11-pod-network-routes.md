# Подготовка сетевых маршрутов подов

Поды, запланированные для узла, получают IP-адрес из CIDR диапазона. В этот момент поды не могут связываться с другими подами, работающими на разных узлах из-за отсутствия сетевых [маршрутов](https://cloud.yandex.ru/docs/vpc/concepts/static-routes).

В этой главе вы создадите маршрут для каждого экземпляра worker узла, который сопоставляет диапазон Pod CIDR узла с внутренним IP-адресом узла.

> Есть и [другие способы](https://kubernetes.io/docs/concepts/cluster-administration/networking/#how-to-achieve-this) реализации сетевой модели Kubernetes.

## Маршруты

Создайте сетевые маршруты для каждого worker узла:

```
yc vpc route-table create \
  --name=kubernetes-route \
  --network-name=kubernetes-the-hard-way \
  --route destination=10.200.0.0/24,next-hop=10.240.0.20 \
  --route destination=10.200.1.0/24,next-hop=10.240.0.21 \
  --route destination=10.200.2.0/24,next-hop=10.240.0.22  
```

Привяжите таблицу маршрутизации к подсети `kubernetes`:

```
yc vpc subnet update kubernetes --route-table-name kubernetes-route
```

Выведите маршруты в `kubernetes-the-hard-way` VPC сети:

```
yc vpc route-table get kubernetes-route
```

> вывод

```
static_routes:
- destination_prefix: 10.200.0.0/24
  next_hop_address: 10.240.0.20
- destination_prefix: 10.200.1.0/24
  next_hop_address: 10.240.0.21
- destination_prefix: 10.200.2.0/24
  next_hop_address: 10.240.0.22
```

Далее: [Развертывание надстройки DNS-кластера](12-dns-addon.md)
