# Подготовка CA и генерация TLS сертификатов

В этой главе вы подготовите [PKI инфраструктуру](https://en.wikipedia.org/wiki/Public_key_infrastructure) с помощью набора инструментов PKI CloudFlare, [cfssl](https://github.com/cloudflare/cfssl), а затем используете ее для развертывания центра сертификации (CA) и создания сертификатов TLS для следующих компонентов:

* etcd,
* kube-apiserver,
* kube-controller-manager,
* kube-scheduler,
* kubelet, 
* kube-proxy.

## Центр Сертификации (СА)

В этом разделе вы подготовите центр сертификации, который будет использоваться для создания TLS сертификатов.

Создайте конфигурационный файл CA, сертификат и приватный ключ:

```bash
{

cat > ca-config.json <<EOF
{
  "signing": {
    "default": {
      "expiry": "8760h"
    },
    "profiles": {
      "kubernetes": {
        "usages": ["signing", "key encipherment", "server auth", "client auth"],
        "expiry": "8760h"
      }
    }
  }
}
EOF

cat > ca-csr.json <<EOF
{
  "CN": "Kubernetes",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "Kubernetes",
      "OU": "CA",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert -initca ca-csr.json | cfssljson -bare ca

}
```

Результат:

```bash
ca-key.pem
ca.pem
```

## Сертификаты для клиента и сервера

В этом разделе вы создадите сертификаты клиента и сервера для каждого компонента Kubernetes и сертификат клиента для пользователя Kubernetes `admin`.

### Admin Client сертификат

Сгенерируйте клиентский сертификат `admin` и приватный ключ:

```bash
{

cat > admin-csr.json <<EOF
{
  "CN": "admin",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:masters",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -profile=kubernetes \
  admin-csr.json | cfssljson -bare admin

}
```

Результат:

```bash
admin-key.pem
admin.pem
```

### Kubelet Client сертификаты

Kubernetes использует [специальный режим авторизации](https://kubernetes.io/docs/admin/authorization/node/), называемый Node Authorizer, который авторизует запросы API, сделанные [Kubelets](https://kubernetes.io/docs/concepts/overview/components/#kubelet). Чтобы авторизоваться как Node Authorizer, Kubelets должен использовать учетные данные, идентифицирующие их как принадлежащие к группе `system:nodes`, с именем пользователя `system:node:<nodeName>`. В этом разделе вы создадите сертификат для каждой worker ноды Kubernetes, который отвечает требованиям Node Authorizer.


Сгенерируйте сертификат и приватный ключ для каждой worker ноды Kubernetes:

```bash
for instance in worker-0 worker-1 worker-2; do
cat > ${instance}-csr.json <<EOF
{
  "CN": "system:node:${instance}",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:nodes",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

EXTERNAL_IP=$(yc compute instance get ${instance} \
  | grep -A1 one_to_one_nat: \
  | grep -oE '\b[0-9]{1,3}(\.[0-9]{1,3}){3}\b')
  
INTERNAL_IP=$(yc compute instance get ${instance} \
  | grep -A1 primary_v4_address: \
  | grep -oE '\b[0-9]{1,3}(\.[0-9]{1,3}){3}\b')

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -hostname=${instance},${EXTERNAL_IP},${INTERNAL_IP} \
  -profile=kubernetes \
  ${instance}-csr.json | cfssljson -bare ${instance}
done
```

Результат:

```bash
worker-0-key.pem
worker-0.pem
worker-1-key.pem
worker-1.pem
worker-2-key.pem
worker-2.pem
```

### Controller Manager Client сертификат

Сгенерируйте клиентский сертификат `kube-controller-manager` и приватный ключ:

```bash
{

cat > kube-controller-manager-csr.json <<EOF
{
  "CN": "system:kube-controller-manager",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:kube-controller-manager",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -profile=kubernetes \
  kube-controller-manager-csr.json | cfssljson -bare kube-controller-manager

}
```

Результат:

```bash
kube-controller-manager-key.pem
kube-controller-manager.pem
```


### Kube Proxy Client сертификат

Сгенерируйте клиентский сертификат `kube-proxy` и приватный ключ:

```bash
{

cat > kube-proxy-csr.json <<EOF
{
  "CN": "system:kube-proxy",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:node-proxier",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -profile=kubernetes \
  kube-proxy-csr.json | cfssljson -bare kube-proxy

}
```

Результат:

```bash
kube-proxy-key.pem
kube-proxy.pem
```

### Scheduler Client сертификат

Сгенерируйте клиентский сертификат `kube-scheduler` и приватный ключ:

```bash
{

cat > kube-scheduler-csr.json <<EOF
{
  "CN": "system:kube-scheduler",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:kube-scheduler",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -profile=kubernetes \
  kube-scheduler-csr.json | cfssljson -bare kube-scheduler

}
```

Результат:

```bash
kube-scheduler-key.pem
kube-scheduler.pem
```


### Kubernetes API Server сертификат

Статический IP-адрес `kubernetes-the-hard-way` будет включен в список альтернативных имен подлежащих для сертификата Kubernetes API сервера. Это гарантирует, что сертификат может быть проверен удаленными клиентами.

Добавьте в окружение переменную `KUBERNETES_PUBLIC_ADDRESS`, значение которой будет являться зарезервированный статический публичный IP-адрес, созданный в главе [Подготовка виртуальных машин](https://gitlab.com/AlPervakov/yc-kubernetes-the-hart-way/-/blob/master/docs/03-compute-resources.md#публичный-ip-адрес-kubernetes):

```bash
export KUBERNETES_PUBLIC_ADDRESS=ВАШ_ЗАРЕЗЕРВИРОВАННЫЙ_IP
```

Сгенерируйте Kubernetes API Server сертификат и приватный ключ:

```bash
{

KUBERNETES_HOSTNAMES=kubernetes,kubernetes.default,kubernetes.default.svc,kubernetes.default.svc.cluster,kubernetes.svc.cluster.local

cat > kubernetes-csr.json <<EOF
{
  "CN": "kubernetes",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "Kubernetes",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -hostname=10.32.0.1,10.240.0.10,10.240.0.11,10.240.0.12,${KUBERNETES_PUBLIC_ADDRESS},127.0.0.1,${KUBERNETES_HOSTNAMES} \
  -profile=kubernetes \
  kubernetes-csr.json | cfssljson -bare kubernetes

}
```

> Kubernetes API серверу автоматически присваивается внутреннее DNS имя `kubernetes`, которое будет связано с первым IP-адресом (`10.32.0.1`) из диапазона адресов (`10.32.0.0/24`), зарезервированного для внутренних служб кластера во время [создания управляющих компонентов](08-bootstrapping-kubernetes-controllers.md#configure-the-kubernetes-api-server).

Результат:

```bash
kubernetes-key.pem
kubernetes.pem
```

## Пара ключей Service Account

Kubernetes Controller Manager использует пару ключей для создания и подписи токенов учетных записей служб, как описано в документации по [управлению учетными записями служб](https://kubernetes.io/docs/admin/service-accounts-admin/).

Сгенерируйте `service-account` сертификат и приватный ключ:

```bash
{

cat > service-account-csr.json <<EOF
{
  "CN": "service-accounts",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "Kubernetes",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -profile=kubernetes \
  service-account-csr.json | cfssljson -bare service-account

}
```

Результат:

```bash
service-account-key.pem
service-account.pem
```



## Перешлите сертификаты клиента и сервера

Скопируйте соответствующие сертификаты и приватные ключи на каждую worker ноду (worker instance):

```bash
scp ca.pem worker-0-key.pem worker-0.pem yc-user@${EXTERNAL_IP_WORKER_0}:~/
scp ca.pem worker-1-key.pem worker-1.pem yc-user@${EXTERNAL_IP_WORKER_1}:~/
scp ca.pem worker-2-key.pem worker-2.pem yc-user@${EXTERNAL_IP_WORKER_2}:~/
```
> Удостоверьтесь, что данные переменные окружения созданы. Данный шаг выполнялся в разделе [Добавление внешних IP-адресов инстансов в переменные окружения](https://gitlab.com/AlPervakov/yc-kubernetes-the-hart-way/-/blob/master/docs/03-compute-resources.md#добавление-внешних-ip-адресов-инстансов-в-переменные-окружения)

Скопируйте соответствующие сертификаты и приватные ключи для каждого экземпляра master ноды:

```bash
scp ca.pem ca-key.pem kubernetes-key.pem kubernetes.pem \
    service-account-key.pem service-account.pem yc-user@${EXTERNAL_IP_MASTER_0}:~/
scp ca.pem ca-key.pem kubernetes-key.pem kubernetes.pem \
    service-account-key.pem service-account.pem yc-user@${EXTERNAL_IP_MASTER_1}:~/
scp ca.pem ca-key.pem kubernetes-key.pem kubernetes.pem \
    service-account-key.pem service-account.pem yc-user@${EXTERNAL_IP_MASTER_2}:~/
```
> Удостоверьтесь, что переменные окружения созданы. Данный шаг выполнялся в разделе [Добавление внешних IP-адресов инстансов в переменные окружения](https://gitlab.com/AlPervakov/yc-kubernetes-the-hart-way/-/blob/master/docs/03-compute-resources.md#добавление-внешних-ip-адресов-инстансов-в-переменные-окружения)

> `kube-proxy`, `kube-controller-manager`, `kube-scheduler`, и `kubelet` будут использоваться для создания файлов конфигурации аутентификации клиента в следующей главе.

------

Далее: [Генерирование конфигурационныйх файлов Kubernetes для аутентификации](05-kubernetes-configuration-files.md)

