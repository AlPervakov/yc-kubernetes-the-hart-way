# Установка клиентских инструментов

В этой главе описана установка утилит, необходимых для выполнения руководства: [cfssl](https://github.com/cloudflare/cfssl), [cfssljson](https://github.com/cloudflare/cfssl), и [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl).


## Установка CFSSL

Утилиты `cfssl` и `cfssljson` будут использоваться для работы с [PKI Infrastructure](https://en.wikipedia.org/wiki/Public_key_infrastructure) и создания сертификатов TLS. Это позволит подписывать, проверять и объединять TLS сертификаты.

Скачайте и установите `cfssl` и `cfssljson`:

### OS X

```bash
curl -o cfssl https://storage.googleapis.com/kubernetes-the-hard-way/cfssl/darwin/cfssl
curl -o cfssljson https://storage.googleapis.com/kubernetes-the-hard-way/cfssl/darwin/cfssljson
```

```bash
chmod +x cfssl cfssljson
```

```bash
sudo mv cfssl cfssljson /usr/local/bin/
```

У некоторых пользователей OS X могут возникнуть проблемы с использованием предварительно собранных бинарных файлов, в этом случае выходом из данной ситуации может стать [Homebrew](https://brew.sh):

```bash
brew install cfssl
```

### Linux

```bash
wget -q --show-progress --https-only --timestamping \
  https://storage.googleapis.com/kubernetes-the-hard-way/cfssl/linux/cfssl \
  https://storage.googleapis.com/kubernetes-the-hard-way/cfssl/linux/cfssljson
```

```bash
chmod +x cfssl cfssljson
```

```bash
sudo mv cfssl cfssljson /usr/local/bin/
```

### Верификация

Проверьте что `cfssl` и `cfssljson` имеют версию 1.3.4 или выше:

```bash
cfssl version
```

> вывод

```bash
Version: 1.3.4
Revision: dev
Runtime: go1.13
```

```bash
cfssljson --version
```
```bash
Version: 1.3.4
Revision: dev
Runtime: go1.13
```

## Установка kubectl

Инструмент командной строки Kubernetes `kubectl` позволяет запускать команды для кластеров Kubernetes. Вы можете использовать `kubectl` для развертывания приложений, проверки и управления ресурсами кластера, а также для просмотра логов. Полный список операций `kubectl` смотрите в [Overview of kubectl](https://kubernetes.io/docs/reference/kubectl/overview/).
Скачайте и установите `kubectl` с [официального ресурса](https://kubernetes.io/ru/docs/tasks/tools/install-kubectl/):

### OS X

```bash
curl -o kubectl https://storage.googleapis.com/kubernetes-release/release/v1.15.3/bin/darwin/amd64/kubectl
```

```bash
chmod +x kubectl
```

```bash
sudo mv kubectl /usr/local/bin/
```

### Linux

```bash
wget https://storage.googleapis.com/kubernetes-release/release/v1.15.3/bin/linux/amd64/kubectl
```

```bash
chmod +x kubectl
```

```bash
sudo mv kubectl /usr/local/bin/
```

### Верификация

Проверьте, что версия `kubectl` 1.15.3 или выше:

```bash
kubectl version --client
```

> вывод

```bash
Client Version: version.Info{Major:"1", Minor:"15", GitVersion:"v1.15.3", GitCommit:"2d3c76f9091b6bec110a5e63777c332469e0cba2", GitTreeState:"clean", BuildDate:"2019-08-19T11:13:54Z", GoVersion:"go1.12.9", Compiler:"gc", Platform:"linux/amd64"}
```

------

Далее: [Подготовка виртуальных машин](03-compute-resources.md)