# Генерирование конфигурационных файлов Kubernetes для аутентификации

В этой главе вы [создадите файлы конфигурации Kubernetes](https://kubernetes.io/docs/concepts/configuration/organize-cluster-access-kubeconfig/), также известные как kubeconfigs, которые позволяют клиентам Kubernetes находить и проверять подлинность на Kubernetes API серверах.

## Конфигурация аутентификации клиента

В этом разделе вы будете генерировать kubeconfig файлы для клиентов `controller manager`, `kubelet`, `kube-proxy`, `scheduler` и пользователя `admin`.

### Публичный IP-адрес Kubernetes

Каждому kubeconfig требуется Kubernetes API сервер для подключения. Для обеспечения высокой доступности будет использоваться IP-адрес, назначенный внешнему балансировщику нагрузки на Kubernetes API серверах.

Проверьте наличие переменной окружения `KUBERNETES_PUBLIC_ADDRESS` и ее значение, оно должно совпадать с зарезервированным в Яндекс Облаке статическим IP-адресом:

```bash
echo ${KUBERNETES_PUBLIC_ADDRESS}
```

> Посмотреть зарезервированный статический IP-адрес можно на старнице каталога в разделе **Virtual Private Cloud**, на вкладке **IP-адреса**.

Если данной переменной не существует, то следует добавить ее и IP-адрес для дальнейшего использования:

```bash
export KUBERNETES_PUBLIC_ADDRESS=ВАШ_ЗАРЕЗЕРВИРОВАННЫЙ_IP
```

### Конфигурационный файл kubelet

При создании файлов kubeconfig для Kubelets должен использоваться сертификат клиента, соответствующий имени узла Kubelet. Это обеспечит надлежащую авторизацию Kubelets [авторизатором узлов](https://kubernetes.io/docs/admin/authorization/node/) Kubernetes .

> Следующие команды должны выполняться в том же каталоге, который использовался для создания TLS сертификатов в предыдущей главе.

Сгенерируйте kubeconfig файл для каждой worker ноды:

```bash
for instance in worker-0 worker-1 worker-2; do
  kubectl config set-cluster kubernetes-the-hard-way \
    --certificate-authority=ca.pem \
    --embed-certs=true \
    --server=https://${KUBERNETES_PUBLIC_ADDRESS}:6443 \
    --kubeconfig=${instance}.kubeconfig

  kubectl config set-credentials system:node:${instance} \
    --client-certificate=${instance}.pem \
    --client-key=${instance}-key.pem \
    --embed-certs=true \
    --kubeconfig=${instance}.kubeconfig

  kubectl config set-context default \
    --cluster=kubernetes-the-hard-way \
    --user=system:node:${instance} \
    --kubeconfig=${instance}.kubeconfig

  kubectl config use-context default --kubeconfig=${instance}.kubeconfig
done
```

Результат:

```bash
worker-0.kubeconfig
worker-1.kubeconfig
worker-2.kubeconfig
```

### Конфигурационный файл kube-proxy

Сгенерируйте kubeconfig файл для сервиса `kube-proxy`:

```bash
{
  kubectl config set-cluster kubernetes-the-hard-way \
    --certificate-authority=ca.pem \
    --embed-certs=true \
    --server=https://${KUBERNETES_PUBLIC_ADDRESS}:6443 \
    --kubeconfig=kube-proxy.kubeconfig

  kubectl config set-credentials system:kube-proxy \
    --client-certificate=kube-proxy.pem \
    --client-key=kube-proxy-key.pem \
    --embed-certs=true \
    --kubeconfig=kube-proxy.kubeconfig

  kubectl config set-context default \
    --cluster=kubernetes-the-hard-way \
    --user=system:kube-proxy \
    --kubeconfig=kube-proxy.kubeconfig

  kubectl config use-context default --kubeconfig=kube-proxy.kubeconfig
}
```

Результат:

```
kube-proxy.kubeconfig
```

### Конфигурационный файл kube-controller-manager

Сгенерируйте kubeconfig файл для сервиса `kube-controller-manager`:

```bash
{
  kubectl config set-cluster kubernetes-the-hard-way \
    --certificate-authority=ca.pem \
    --embed-certs=true \
    --server=https://127.0.0.1:6443 \
    --kubeconfig=kube-controller-manager.kubeconfig

  kubectl config set-credentials system:kube-controller-manager \
    --client-certificate=kube-controller-manager.pem \
    --client-key=kube-controller-manager-key.pem \
    --embed-certs=true \
    --kubeconfig=kube-controller-manager.kubeconfig

  kubectl config set-context default \
    --cluster=kubernetes-the-hard-way \
    --user=system:kube-controller-manager \
    --kubeconfig=kube-controller-manager.kubeconfig

  kubectl config use-context default --kubeconfig=kube-controller-manager.kubeconfig
}
```

Результат:

```
kube-controller-manager.kubeconfig
```


### Конфигурационный файл kube-scheduler

Сгенерируйте kubeconfig файл для сервиса `kube-scheduler`:

```bash
{
  kubectl config set-cluster kubernetes-the-hard-way \
    --certificate-authority=ca.pem \
    --embed-certs=true \
    --server=https://127.0.0.1:6443 \
    --kubeconfig=kube-scheduler.kubeconfig

  kubectl config set-credentials system:kube-scheduler \
    --client-certificate=kube-scheduler.pem \
    --client-key=kube-scheduler-key.pem \
    --embed-certs=true \
    --kubeconfig=kube-scheduler.kubeconfig

  kubectl config set-context default \
    --cluster=kubernetes-the-hard-way \
    --user=system:kube-scheduler \
    --kubeconfig=kube-scheduler.kubeconfig

  kubectl config use-context default --kubeconfig=kube-scheduler.kubeconfig
}
```

Результат:

```
kube-scheduler.kubeconfig
```

### Конфигурационный файл admin

Сгенерируйте kubeconfig файл для пользователя `admin`:

```bash
{
  kubectl config set-cluster kubernetes-the-hard-way \
    --certificate-authority=ca.pem \
    --embed-certs=true \
    --server=https://127.0.0.1:6443 \
    --kubeconfig=admin.kubeconfig

  kubectl config set-credentials admin \
    --client-certificate=admin.pem \
    --client-key=admin-key.pem \
    --embed-certs=true \
    --kubeconfig=admin.kubeconfig

  kubectl config set-context default \
    --cluster=kubernetes-the-hard-way \
    --user=admin \
    --kubeconfig=admin.kubeconfig

  kubectl config use-context default --kubeconfig=admin.kubeconfig
}
```

Результат:

```bash
admin.kubeconfig
```

------



## Перешлите конфигурационные файлы Kubernetes

Скопируйте соответствующие конфигурационые файлы `kubelet` и `kube-proxy` на каждую worker ноду:

```bash
scp worker-0.kubeconfig kube-proxy.kubeconfig yc-user@${EXTERNAL_IP_WORKER_0}:~/
scp worker-1.kubeconfig kube-proxy.kubeconfig yc-user@${EXTERNAL_IP_WORKER_1}:~/
scp worker-2.kubeconfig kube-proxy.kubeconfig yc-user@${EXTERNAL_IP_WORKER_2}:~/
```

> Удостоверьтесь, что данные переменные окружения созданы. Данный шаг выполнялся в разделе [Добавление внешних IP-адресов инстансов в переменные окружения](https://gitlab.com/AlPervakov/yc-kubernetes-the-hart-way/-/blob/master/docs/03-compute-resources.md#добавление-внешних-ip-адресов-инстансов-в-переменные-окружения)

Скопируйте соответствующие конфигурационные файлы `kube-controller-manager` и `kube-scheduler`  для каждого экземпляра master ноды:

```bash
scp admin.kubeconfig kube-controller-manager.kubeconfig kube-scheduler.kubeconfig yc-user@${EXTERNAL_IP_MASTER_0}:~/
scp admin.kubeconfig kube-controller-manager.kubeconfig kube-scheduler.kubeconfig yc-user@${EXTERNAL_IP_MASTER_1}:~/
scp admin.kubeconfig kube-controller-manager.kubeconfig kube-scheduler.kubeconfig yc-user@${EXTERNAL_IP_MASTER_2}:~/
```

> Удостоверьтесь, что данные переменные окружения созданы. Данный шаг выполнялся в разделе [Добавление внешних IP-адресов инстансов в переменные окружения](https://gitlab.com/AlPervakov/yc-kubernetes-the-hart-way/-/blob/master/docs/03-compute-resources.md#добавление-внешних-ip-адресов-инстансов-в-переменные-окружения)

------

Далее: [Создание конфигурационного файла и ключа шифрования данных](06-data-encryption-keys.md)
