# Развертывание управляющих компонентов Kubernetes

В этой главе вы развернете управляющие компоненты Kubernetes на трех ВМ и сконфигурируете их для высокой доступности. Вы также создадите внешний балансировщик нагрузки, который предоставляет доступ к Kubernetes API серверам для удаленных клиентов. На каждом управляющем узле будут установлены следующие компоненты: Kubernetes API сервер, планировщик и диспетчер контроллеров.

## Предпосылки

Команды в этой лабораторной работе , должны выполняться на каждом экземпляре master ноды: `master-0`, `master-1` и `master-2`. Войдите в каждый экземпляр мастера с помощью ssh. Пример:

```bash
ssh -i ~/.ssh/id_rsa yc-user@EXTERNAL_IP
```

> где `EXTERNAL_IP` - внешний IP-адрес вашей master ноды


### Параллельный запуск команд с помощью tmux

[tmux](https://github.com/tmux/tmux/wiki) можно использовать для одновременного запуска команд на нескольких ВМ. Смотрите раздел [Параллельный запуск команд с использованием tmux](https://gitlab.com/AlPervakov/yc-kubernetes-the-hart-way/-/blob/master/docs/01-prerequisites.md#параллельный-запуск-команд-с-использованием-tmux).

## Подготовка управляющих компонентов Kubernetes

Создайте каталог конфигурации Kubernetes:

```bash
sudo mkdir -p /etc/kubernetes/config
```

### Загрузите и установите бинарные файлы контроллера Kubernetes

Загрузите официальные релизы Kubernetes:

```bash
wget -q --show-progress --https-only --timestamping \
  "https://storage.googleapis.com/kubernetes-release/release/v1.15.3/bin/linux/amd64/kube-apiserver" \
  "https://storage.googleapis.com/kubernetes-release/release/v1.15.3/bin/linux/amd64/kube-controller-manager" \
  "https://storage.googleapis.com/kubernetes-release/release/v1.15.3/bin/linux/amd64/kube-scheduler" \
  "https://storage.googleapis.com/kubernetes-release/release/v1.15.3/bin/linux/amd64/kubectl"
```

Установите бинарные файлы Kubernetes:

```bash
{
  chmod +x kube-apiserver kube-controller-manager kube-scheduler kubectl
  sudo mv kube-apiserver kube-controller-manager kube-scheduler kubectl /usr/local/bin/
}
```

### Настройте Kubernetes API сервер

```bash
{
  sudo mkdir -p /var/lib/kubernetes/

  sudo mv ca.pem ca-key.pem kubernetes-key.pem kubernetes.pem \
    service-account-key.pem service-account.pem \
    encryption-config.yaml /var/lib/kubernetes/
}
```

Внутренний IP-адрес ВМ будет использоваться для объявления API сервера компонентам кластера. [Получите внутренний IP-адрес](https://cloud.yandex.ru/docs/compute/operations/vm-info/get-info#gce-metadata) для текущей ВМ:

```bash
INTERNAL_IP=$(curl -s -H "Metadata-Flavor: Google" \
  169.254.169.254/computeMetadata/v1/instance/network-interfaces/0/ip)
```

Создайте systemd файл `kube-apiserver.service`:

```bash
cat <<EOF | sudo tee /etc/systemd/system/kube-apiserver.service
[Unit]
Description=Kubernetes API Server
Documentation=https://github.com/kubernetes/kubernetes

[Service]
ExecStart=/usr/local/bin/kube-apiserver \\
  --advertise-address=${INTERNAL_IP} \\
  --allow-privileged=true \\
  --apiserver-count=3 \\
  --audit-log-maxage=30 \\
  --audit-log-maxbackup=3 \\
  --audit-log-maxsize=100 \\
  --audit-log-path=/var/log/audit.log \\
  --authorization-mode=Node,RBAC \\
  --bind-address=0.0.0.0 \\
  --client-ca-file=/var/lib/kubernetes/ca.pem \\
  --enable-admission-plugins=NamespaceLifecycle,NodeRestriction,LimitRanger,ServiceAccount,DefaultStorageClass,ResourceQuota \\
  --etcd-cafile=/var/lib/kubernetes/ca.pem \\
  --etcd-certfile=/var/lib/kubernetes/kubernetes.pem \\
  --etcd-keyfile=/var/lib/kubernetes/kubernetes-key.pem \\
  --etcd-servers=https://10.240.0.10:2379,https://10.240.0.11:2379,https://10.240.0.12:2379 \\
  --event-ttl=1h \\
  --encryption-provider-config=/var/lib/kubernetes/encryption-config.yaml \\
  --kubelet-certificate-authority=/var/lib/kubernetes/ca.pem \\
  --kubelet-client-certificate=/var/lib/kubernetes/kubernetes.pem \\
  --kubelet-client-key=/var/lib/kubernetes/kubernetes-key.pem \\
  --kubelet-https=true \\
  --runtime-config=api/all \\
  --service-account-key-file=/var/lib/kubernetes/service-account.pem \\
  --service-cluster-ip-range=10.32.0.0/24 \\
  --service-node-port-range=30000-32767 \\
  --tls-cert-file=/var/lib/kubernetes/kubernetes.pem \\
  --tls-private-key-file=/var/lib/kubernetes/kubernetes-key.pem \\
  --v=2
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF
```

### Настройте диспетчер контроллеров Kubernetes

Переместите `kube-controller-manager` kubeconfig в его рабочую директорию:

```bash
sudo mv kube-controller-manager.kubeconfig /var/lib/kubernetes/
```

Создайте systemd файл `kube-controller-manager.service`:

```bash
cat <<EOF | sudo tee /etc/systemd/system/kube-controller-manager.service
[Unit]
Description=Kubernetes Controller Manager
Documentation=https://github.com/kubernetes/kubernetes

[Service]
ExecStart=/usr/local/bin/kube-controller-manager \\
  --address=0.0.0.0 \\
  --cluster-cidr=10.200.0.0/16 \\
  --cluster-name=kubernetes \\
  --cluster-signing-cert-file=/var/lib/kubernetes/ca.pem \\
  --cluster-signing-key-file=/var/lib/kubernetes/ca-key.pem \\
  --kubeconfig=/var/lib/kubernetes/kube-controller-manager.kubeconfig \\
  --leader-elect=true \\
  --root-ca-file=/var/lib/kubernetes/ca.pem \\
  --service-account-private-key-file=/var/lib/kubernetes/service-account-key.pem \\
  --service-cluster-ip-range=10.32.0.0/24 \\
  --use-service-account-credentials=true \\
  --v=2
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF
```

### Настройте планировщик Kubernetes

Переместите `kube-scheduler` kubeconfig в его рабочую директорию:

```bash
sudo mv kube-scheduler.kubeconfig /var/lib/kubernetes/
```

Создайте конфигурационный файл `kube-scheduler.yaml`:

```bash
cat <<EOF | sudo tee /etc/kubernetes/config/kube-scheduler.yaml
apiVersion: kubescheduler.config.k8s.io/v1alpha1
kind: KubeSchedulerConfiguration
clientConnection:
  kubeconfig: "/var/lib/kubernetes/kube-scheduler.kubeconfig"
leaderElection:
  leaderElect: true
EOF
```

Создайте systemd файл `kube-scheduler.service`:

```bash
cat <<EOF | sudo tee /etc/systemd/system/kube-scheduler.service
[Unit]
Description=Kubernetes Scheduler
Documentation=https://github.com/kubernetes/kubernetes

[Service]
ExecStart=/usr/local/bin/kube-scheduler \\
  --config=/etc/kubernetes/config/kube-scheduler.yaml \\
  --v=2
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF
```

### Запустите сервисы контроллера

```bash
{
  sudo systemctl daemon-reload
  sudo systemctl enable kube-apiserver kube-controller-manager kube-scheduler
  sudo systemctl start kube-apiserver kube-controller-manager kube-scheduler
}
```

> Полная инициализация Kubernetes API сервера может занимать до 10 секунд.

### Включение проверки работоспособности HTTP (Health Checks)

[Yansex Load Balancer](https://cloud.yandex.ru/docs/load-balancer/quickstart) будет использоваться для распределения трафика по трем API серверам и разрешать каждому API серверу прерыватьTLS соединение и проверять клиентские сертификаты. Балансировщик сетевой нагрузки поддерживает только проверки работоспособности HTTP, это означает, что конечная точка HTTPS, предоставляемая сервером API, не может использоваться. В качестве обходного пути можно использовать веб-сервер nginx для проверки работоспособности HTTP-прокси. В этом разделе nginx будет установлен и настроен на прием проверок работоспособности HTTP для порта `80` и прокси-соединений на API сервере `https://127.0.0.1:6443/healthz`.

> Конечная точка  API сервера  `/healthz` не требует проверки подлинности по умолчанию.

Установите базовый веб-сервер для обработки проверок состояния HTTP:

```bash
{
  sudo apt-get update
  sudo apt-get install -y nginx
}
```

```bash
cat > kubernetes.default.svc.cluster.local <<EOF
server {
  listen      80;
  server_name kubernetes.default.svc.cluster.local;

  location /healthz {
     proxy_pass                    https://127.0.0.1:6443/healthz;
     proxy_ssl_trusted_certificate /var/lib/kubernetes/ca.pem;
  }
}
EOF
```

```bash
{
  sudo mv kubernetes.default.svc.cluster.local \
    /etc/nginx/sites-available/kubernetes.default.svc.cluster.local

  sudo ln -s /etc/nginx/sites-available/kubernetes.default.svc.cluster.local /etc/nginx/sites-enabled/
}
```

```bash
sudo systemctl restart nginx
```

```bash
sudo systemctl enable nginx
```

### Верификация

```bash
kubectl get componentstatuses --kubeconfig admin.kubeconfig
```

```bash
NAME                 STATUS    MESSAGE              ERROR
controller-manager   Healthy   ok
scheduler            Healthy   ok
etcd-2               Healthy   {"health": "true"}
etcd-0               Healthy   {"health": "true"}
etcd-1               Healthy   {"health": "true"}
```

Протестируйте nginx HTTP проверки работоспособности proxy:

```bash
curl -H "Host: kubernetes.default.svc.cluster.local" -i http://127.0.0.1/healthz
```

```bash
HTTP/1.1 200 OK
Server: nginx/1.14.0 (Ubuntu)
Date: Sat, 14 Sep 2019 18:34:11 GMT
Content-Type: text/plain; charset=utf-8
Content-Length: 2
Connection: keep-alive
X-Content-Type-Options: nosniff

ok
```

> Не забудьте, что указанные выше команды должны быть выполнены на каждом узле: `master-0`, `master-1` и `master-2`.

## RBAC для авторизации Kubelet

В этом разделе вы настроите разрешения RBAC, чтобы Kubernetes API сервер мог обращаться к API Kubelet на каждом рабочем узле. Доступ к API Kubelet необходим для получения метрик, логов и выполнения команд на подах.

> Это руководство устанавливает Kubelet флаг `--authorization-mode` в `Webhook`. Режим Webhook используется API [SubjectAccessReview](https://kubernetes.io/docs/admin/authorization/#checking-api-access) для определения авторизации.

***Команды в этом разделе влияют на весь кластер, и их нужно запускать только один раз с одного экземпляра master ноды.***

```bash
ssh yc-user@${EXTERNAL_IP_MASTER_0}
```

> Переменная окружения `EXTERNAL_IP_MASTER_0` должна быть создана, см. раздел [Добавление внешних IP-адресов инстансов в переменные окружения](https://gitlab.com/AlPervakov/yc-kubernetes-the-hart-way/-/blob/master/docs/03-compute-resources.md#добавление-внешних-ip-адресов-инстансов-в-переменные-окружения)

Создайте `system:kube-apiserver-to-kubelet` [ClusterRole](https://kubernetes.io/docs/admin/authorization/rbac/#role-and-clusterrole) с разрешениями для доступа к API-интерфейсу Kubelet и выполнения наиболее распространенных задач, связанных с управлением подами:

```bash
cat <<EOF | kubectl apply --kubeconfig admin.kubeconfig -f -
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRole
metadata:
  annotations:
    rbac.authorization.kubernetes.io/autoupdate: "true"
  labels:
    kubernetes.io/bootstrapping: rbac-defaults
  name: system:kube-apiserver-to-kubelet
rules:
  - apiGroups:
      - ""
    resources:
      - nodes/proxy
      - nodes/stats
      - nodes/log
      - nodes/spec
      - nodes/metrics
    verbs:
      - "*"
EOF
```

Kubernetes API сервер аутентифицируется в Kubelet как пользователь `kubernetes`, используя сертификат клиента, как определено флагом `--kubelet-client-certificate` .

Привяжите `system:kube-apiserver-to-kubelet` ClusterRole к пользователю `kubernetes`:

```bash
cat <<EOF | kubectl apply --kubeconfig admin.kubeconfig -f -
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: system:kube-apiserver
  namespace: ""
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: system:kube-apiserver-to-kubelet
subjects:
  - apiGroup: rbac.authorization.k8s.io
    kind: User
    name: kubernetes
EOF
```

## Балансировщик нагрузки внешнего интерфейса Kubernetes

В этом разделе вы подготовите внешний балансировщик нагрузки для Kubernetes API серверов. `kubernetes-the-hard-way` статический IP-адрес, зарезервированный ранее в Яндекс Облаке, будет прикреплен к созданному балансировщику нагрузки.

> **Представленные ниже команды необходимо выполнять с той же машины, которая использовалась для создания ВМ**. ВМ, созданные в этом руководстве, не имеют прав, для того, чтобы вы могли закончить этот раздел. 


### Подготовка балансировщика сетевой нагрузки
[Создайте целевую группу](https://cloud.yandex.ru/docs/load-balancer/operations/target-group-create) из узлов `master-0`, `master-1`, `master-2`:

- узнайте id подсети ВМ (она должна быть одинаковая для всех трех master узлов):

```bash
yc compute instance get master-0 \
  | grep subnet_id: \
  | sed 's/subnet_id: //'
```
- замените `SUBNET_ID` на ваш id подсети и выполните команду:

```bash
yc load-balancer target-group create \
  --name kubernetes-target-pool \
  --region-id ru-central1 \
  --target subnet-id=SUBNET_ID,address=10.240.0.10 \
  --target subnet-id=SUBNET_ID,address=10.240.0.11 \
  --target subnet-id=SUBNET_ID,address=10.240.0.12
```

* добавьте id целевой группы в переменную окружения для дальнейшего использования при создании балансировщика нагрузки:

```shell
export ID_TARGET_GROUP=$(yc load-balancer target-group get kubernetes-target-pool \
  | grep -w id: \
  | sed 's/id: //')
```

Проверьте наличие переменной окружения `KUBERNETES_PUBLIC_ADDRESS` и ее значение, оно должно совпадать с зарезервированным в Яндекс Облаке статическим IP-адресом:

```bash
echo ${KUBERNETES_PUBLIC_ADDRESS}
```

> Посмотреть зарезервированный статический IP-адрес можно на странице каталога в разделе **Virtual Private Cloud**, на вкладке **IP-адреса**.

Если данной переменной не существует, то следует добавить ее и IP-адрес для дальнейшего использования:

```bash
export KUBERNETES_PUBLIC_ADDRESS=ВАШ_ЗАРЕЗЕРВИРОВАННЫЙ_IP
```

[Создайте балансировщик](https://cloud.yandex.ru/docs/load-balancer/operations/load-balancer-create) и прикрепите к нему вашу целевую группу:

```shell
yc load-balancer network-load-balancer create \
--name kubernetes-load-balancer \
--region-id ru-central1 \
--listener name=kubernetes-listener,external-ip-version=ipv4,external-address=$KUBERNETES_PUBLIC_ADDRESS,port=6443 \
--target-group target-group-id=$ID_TARGET_GROUP,healthcheck-name=kubernetes-health-check,healthcheck-interval=2s,healthcheck-timeout=1s,healthcheck-unhealthythreshold=2,healthcheck-healthythreshold=2,healthcheck-http-port=80,healthcheck-http-path=/
```



### Верификация

> **Представленные ниже команды необходимо выполнять с той же машины, которая использовалась для создания ВМ**. ВМ, созданные в этом руководстве, не имеют прав, для того, чтобы вы могли закончить этот раздел.

Сделайте HTTP-запрос для получения информации о версии Kubernetes:

```bash
curl --cacert ca.pem https://${KUBERNETES_PUBLIC_ADDRESS}:6443/version
```

> вывод

```bash
{
  "major": "1",
  "minor": "15",
  "gitVersion": "v1.15.3",
  "gitCommit": "2d3c76f9091b6bec110a5e63777c332469e0cba2",
  "gitTreeState": "clean",
  "buildDate": "2019-08-19T11:05:50Z",
  "goVersion": "go1.12.9",
  "compiler": "gc",
  "platform": "linux/amd64"
}
```
------

Далее: [Развертывание worker нод Kubernetes](09-bootstrapping-kubernetes-workers.md)
