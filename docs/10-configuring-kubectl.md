# Настройка kubectl для удаленного доступа

В этой главе вы создадите файл kubeconfig для утилиты командной строки `kubectl` на основе учетных данных пользователя `admin`.

> Команды в этой лабораторной работе нужно запускать из того же каталога, который использовался для создания клиентских сертификатов администратора.

## Конфигурационный файл администратора Kubernetes

Каждому kubeconfig требуется Kubernetes API сервер для подключения. Для обеспечения высокой доступности будет использоваться IP-адрес, назначенный внешнему балансировщику нагрузки для Kubernetes API серверов.

Проверьте наличие переменной окружения `KUBERNETES_PUBLIC_ADDRESS` и ее значение, оно должно совпадать с зарезервированным в Яндекс Облаке статическим IP-адресом:

```bash
echo ${KUBERNETES_PUBLIC_ADDRESS}
```

> Посмотреть зарезервированный статический IP-адрес можно на странице каталога в разделе **Virtual Private Cloud**, на вкладке **IP-адреса**.

Если данной переменной не существует, то следует добавить ее и IP-адрес для дальнейшего использования:

```bash
export KUBERNETES_PUBLIC_ADDRESS=ВАШ_ЗАРЕЗЕРВИРОВАННЫЙ_IP
```

Создайте kubeconfig файл подходящий для аутентификации пользователя `admin`:

```
{
  kubectl config set-cluster kubernetes-the-hard-way \
    --certificate-authority=ca.pem \
    --embed-certs=true \
    --server=https://${KUBERNETES_PUBLIC_ADDRESS}:6443

  kubectl config set-credentials admin \
    --client-certificate=admin.pem \
    --client-key=admin-key.pem

  kubectl config set-context kubernetes-the-hard-way \
    --cluster=kubernetes-the-hard-way \
    --user=admin

  kubectl config use-context kubernetes-the-hard-way
}
```

## Верификация

Проверьте работоспособность удаленного кластера Kubernetes:

```
kubectl get componentstatuses
```

> вывод

```
NAME                 STATUS    MESSAGE             ERROR
controller-manager   Healthy   ok
scheduler            Healthy   ok
etcd-1               Healthy   {"health":"true"}
etcd-2               Healthy   {"health":"true"}
etcd-0               Healthy   {"health":"true"}
```

Получите список узлов в удаленном кластере Kubernetes:

```
kubectl get nodes
```

> вывод

```
NAME       STATUS   ROLES    AGE    VERSION
worker-0   Ready    <none>   2m9s   v1.15.3
worker-1   Ready    <none>   2m9s   v1.15.3
worker-2   Ready    <none>   2m9s   v1.15.3
```

------

Далее: [Подготовка сетевых маршрутов подов](11-pod-network-routes.md)
