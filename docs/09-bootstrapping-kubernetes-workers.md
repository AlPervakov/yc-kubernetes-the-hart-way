# Развертывание worker нод Kubernetes

В этой главе вы развернете три рабочих узла Kubernetes. Следующие компоненты будут установлены на каждом экземпляре worker ноды: [RunC](https://github.com/opencontainers/runc) , [контейнер сетевых плагинов](https://github.com/containernetworking/cni) , [containerd](https://github.com/containerd/containerd) , [kubelet](https://kubernetes.io/docs/admin/kubelet) и [kube-proxy](https://kubernetes.io/docs/concepts/cluster-administration/proxies).

## Предпосылки

Команды в этой лабораторной работе , должны выполняться на каждом экземпляре worker ноды: `worker-0`, `worker-1` и `worker-2`. Войдите в каждый экземпляр контроллера с помощью ssh. Пример:

```bash
ssh -i ~/.ssh/id_rsa yc-user@EXTERNAL_IP
```

> где `EXTERNAL_IP` - внешний IP-адрес вашей worker ноды


### Параллельный запуск команд с помощью tmux

[tmux](https://github.com/tmux/tmux/wiki) можно использовать для одновременного запуска команд на нескольких ВМ. Смотрите раздел [Параллельный запуск команд с использованием tmux](https://gitlab.com/AlPervakov/yc-kubernetes-the-hart-way/-/blob/master/docs/01-prerequisites.md#параллельный-запуск-команд-с-использованием-tmux).

## Подготовка worker нод Kubernetes

Установите зависимости ОС:

```bash
{
  sudo apt-get update
  sudo apt-get -y install socat conntrack ipset
}
```

> Бинарный файл socat обеспечивает поддержку команды `kubectl port-forward`.

### Отключите Swap

По умолчанию кублет не запустится, если включен [swap](https://help.ubuntu.com/community/SwapFaq). [Рекомендуется](https://github.com/kubernetes/kubernetes/issues/7294), чтобы swap был отключен, чтобы Kubernetes мог обеспечить надлежащее распределение ресурсов и качество обслуживания.

Проверьте, включен ли swap:

```bash
sudo swapon --show
```

Если вывод пуст, то swap отключен. Если swap включен, выполните следующую команду, чтобы его отключить:

```bash
sudo swapoff -a
```

> Чтобы swap остался выключенным после перезагрузки, обратитесь к документации по дистрибутиву Linux.

### Загрузите и установите бинарные файлы Kubernetes для worker нод

```bash
wget -q --show-progress --https-only --timestamping \
  https://github.com/kubernetes-sigs/cri-tools/releases/download/v1.15.0/crictl-v1.15.0-linux-amd64.tar.gz \
  https://github.com/opencontainers/runc/releases/download/v1.0.0-rc8/runc.amd64 \
  https://github.com/containernetworking/plugins/releases/download/v0.8.2/cni-plugins-linux-amd64-v0.8.2.tgz \
  https://github.com/containerd/containerd/releases/download/v1.2.9/containerd-1.2.9.linux-amd64.tar.gz \
  https://storage.googleapis.com/kubernetes-release/release/v1.15.3/bin/linux/amd64/kubectl \
  https://storage.googleapis.com/kubernetes-release/release/v1.15.3/bin/linux/amd64/kube-proxy \
  https://storage.googleapis.com/kubernetes-release/release/v1.15.3/bin/linux/amd64/kubelet
```

Создайте каталоги для установки:

```bash
sudo mkdir -p \
  /etc/cni/net.d \
  /opt/cni/bin \
  /var/lib/kubelet \
  /var/lib/kube-proxy \
  /var/lib/kubernetes \
  /var/run/kubernetes
```

Установите бинарные файлы:

```bash
{
  mkdir containerd
  tar -xvf crictl-v1.15.0-linux-amd64.tar.gz
  tar -xvf containerd-1.2.9.linux-amd64.tar.gz -C containerd
  sudo tar -xvf cni-plugins-linux-amd64-v0.8.2.tgz -C /opt/cni/bin/
  sudo mv runc.amd64 runc
  chmod +x crictl kubectl kube-proxy kubelet runc 
  sudo mv crictl kubectl kube-proxy kubelet runc /usr/local/bin/
  sudo mv containerd/bin/* /bin/
}
```

### Настройте CNI сеть

Получите диапазон Pod CIDR для текущей ВМ:

```bash
POD_CIDR=$(curl -s -H "Metadata-Flavor: Google" \
  169.254.169.254/computeMetadata/v1/instance/attributes/pod-cidr)
```

Создайте конфигурационный файл сети `bridge`:

```bash
cat <<EOF | sudo tee /etc/cni/net.d/10-bridge.conf
{
    "cniVersion": "0.3.1",
    "name": "bridge",
    "type": "bridge",
    "bridge": "cnio0",
    "isGateway": true,
    "ipMasq": true,
    "ipam": {
        "type": "host-local",
        "ranges": [
          [{"subnet": "${POD_CIDR}"}]
        ],
        "routes": [{"dst": "0.0.0.0/0"}]
    }
}
EOF
```

Создайте конфигурационный файл сети `loopback`:

```bash
cat <<EOF | sudo tee /etc/cni/net.d/99-loopback.conf
{
    "cniVersion": "0.3.1",
    "name": "lo",
    "type": "loopback"
}
EOF
```

### Настройка containerd

Создайте конфигурационный файл `containerd`:

```bash
sudo mkdir -p /etc/containerd/
```

```bash
cat << EOF | sudo tee /etc/containerd/config.toml
[plugins]
  [plugins.cri.containerd]
    snapshotter = "overlayfs"
    [plugins.cri.containerd.default_runtime]
      runtime_type = "io.containerd.runtime.v1.linux"
      runtime_engine = "/usr/local/bin/runc"
      runtime_root = ""
EOF
```

Создайте systemd файл `containerd.service`:

```bash
cat <<EOF | sudo tee /etc/systemd/system/containerd.service
[Unit]
Description=containerd container runtime
Documentation=https://containerd.io
After=network.target

[Service]
ExecStartPre=/sbin/modprobe overlay
ExecStart=/bin/containerd
Restart=always
RestartSec=5
Delegate=yes
KillMode=process
OOMScoreAdjust=-999
LimitNOFILE=1048576
LimitNPROC=infinity
LimitCORE=infinity

[Install]
WantedBy=multi-user.target
EOF
```

### Настройка Kubelet

```bash
{
  sudo mv ${HOSTNAME}-key.pem ${HOSTNAME}.pem /var/lib/kubelet/
  sudo mv ${HOSTNAME}.kubeconfig /var/lib/kubelet/kubeconfig
  sudo mv ca.pem /var/lib/kubernetes/
}
```

Создайте конфигурационный файл `kubelet-config.yaml`:

```bash
cat <<EOF | sudo tee /var/lib/kubelet/kubelet-config.yaml
kind: KubeletConfiguration
apiVersion: kubelet.config.k8s.io/v1beta1
authentication:
  anonymous:
    enabled: false
  webhook:
    enabled: true
  x509:
    clientCAFile: "/var/lib/kubernetes/ca.pem"
authorization:
  mode: Webhook
clusterDomain: "cluster.local"
clusterDNS:
  - "10.32.0.10"
podCIDR: "${POD_CIDR}"
resolvConf: "/run/systemd/resolve/resolv.conf"
runtimeRequestTimeout: "15m"
tlsCertFile: "/var/lib/kubelet/${HOSTNAME}.pem"
tlsPrivateKeyFile: "/var/lib/kubelet/${HOSTNAME}-key.pem"
EOF
```

> Конфигурация `resolvConf` используется, чтобы избежать зацикливания при использовании CoreDNS для обнаружения служб в работающих системах `systemd-resolved`.

Создайте systemd файл `kubelet.service`:

```bash
cat <<EOF | sudo tee /etc/systemd/system/kubelet.service
[Unit]
Description=Kubernetes Kubelet
Documentation=https://github.com/kubernetes/kubernetes
After=containerd.service
Requires=containerd.service

[Service]
ExecStart=/usr/local/bin/kubelet \\
  --config=/var/lib/kubelet/kubelet-config.yaml \\
  --container-runtime=remote \\
  --container-runtime-endpoint=unix:///var/run/containerd/containerd.sock \\
  --image-pull-progress-deadline=2m \\
  --kubeconfig=/var/lib/kubelet/kubeconfig \\
  --network-plugin=cni \\
  --register-node=true \\
  --v=2
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF
```

### Конфигурация Kubernetes Proxy

```bash
sudo mv kube-proxy.kubeconfig /var/lib/kube-proxy/kubeconfig
```

Создайте конфигурационный файл `kube-proxy-config.yaml`:

```bash
cat <<EOF | sudo tee /var/lib/kube-proxy/kube-proxy-config.yaml
kind: KubeProxyConfiguration
apiVersion: kubeproxy.config.k8s.io/v1alpha1
clientConnection:
  kubeconfig: "/var/lib/kube-proxy/kubeconfig"
mode: "iptables"
clusterCIDR: "10.200.0.0/16"
EOF
```

Создайте systemd файл `kube-proxy.service`:

```bash
cat <<EOF | sudo tee /etc/systemd/system/kube-proxy.service
[Unit]
Description=Kubernetes Kube Proxy
Documentation=https://github.com/kubernetes/kubernetes

[Service]
ExecStart=/usr/local/bin/kube-proxy \\
  --config=/var/lib/kube-proxy/kube-proxy-config.yaml
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF
```

### Запустите сервисы на worker нодах

```bash
{
  sudo systemctl daemon-reload
  sudo systemctl enable containerd kubelet kube-proxy
  sudo systemctl start containerd kubelet kube-proxy
}
```

> Не забудьте выполнить вышеуказанные команды на каждом узле: `worker-0`, `worker-1` и `worker-2`.

## Верификация

> **Представленные ниже команды необходимо выполнять с той же машины, которая использовалась для создания ВМ**. ВМ, созданные в этом руководстве, не имеют прав, для того, чтобы вы могли закончить этот раздел. 

Подключимся к master ноде для получения списка зарегистрированных узлов Kubernetes:

```bash
ssh yc-user@${EXTERNAL_IP_MASTER_0}
```

> Переменная окружения `EXTERNAL_IP_MASTER_0` должна быть создана, см [Добавление внешних IP-адресов инстансов в переменные окружения](https://gitlab.com/AlPervakov/yc-kubernetes-the-hart-way/-/blob/master/docs/03-compute-resources.md#добавление-внешних-ip-адресов-инстансов-в-переменные-окружения)

```bash
yc-user@master-0:~$ kubectl get nodes --kubeconfig admin.kubeconfig
```

> вывод

```bash
NAME       STATUS   ROLES    AGE   VERSION
worker-0   Ready    <none>   15s   v1.15.3
worker-1   Ready    <none>   15s   v1.15.3
worker-2   Ready    <none>   15s   v1.15.3
```

------

Далее: [Настройка kubectl для удаленного доступа](10-configuring-kubectl.md)
