# Развертывание надстройки DNS-кластера

В этой главе вы развернете [DNS надстройку](https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/), которая обеспечивает обнаружение службы на основе DNS, при поддержке [CoreDNS](https://coredns.io/), для приложений, работающих в кластере Kubernetes.

## Надстройка DNS-кластера

Разверните надстройку кластера `coredns`:

```bash
kubectl apply -f https://storage.googleapis.com/kubernetes-the-hard-way/coredns.yaml
```

> вывод

```bash
serviceaccount/coredns created
clusterrole.rbac.authorization.k8s.io/system:coredns created
clusterrolebinding.rbac.authorization.k8s.io/system:coredns created
configmap/coredns created
deployment.extensions/coredns created
service/kube-dns created
```

Выведите список подов, созданных при разворачивании `kube-dns`:

```bash
kubectl get pods -l k8s-app=kube-dns -n kube-system
```

> вывод

```bash
NAME                     READY   STATUS    RESTARTS   AGE
coredns-5fb99965-cd65k   1/1     Running   0          19s
coredns-5fb99965-k9cvz   1/1     Running   0          19s
```

## Верификация

Разверните  `busybox`:

```bash
kubectl run --generator=run-pod/v1 busybox --image=busybox:1.28 --command -- sleep 3600
```

Выведите список подов, созданных при развертывании `busybox`:

```bash
kubectl get pods -l run=busybox
```

> вывод

```bash
NAME      READY   STATUS    RESTARTS   AGE
busybox   1/1     Running   0          4s
```

Получите полное имя пода `busybox`:

```bash
POD_NAME=$(kubectl get pods -l run=busybox -o jsonpath="{.items[0].metadata.name}")
```

Выполните поиск DNS для  `kubernetes` сервиса внутри пода `busybox`:

```bash
kubectl exec -ti $POD_NAME -- nslookup kubernetes
```

> вывод

```bash
Server:    10.32.0.10
Address 1: 10.32.0.10 kube-dns.kube-system.svc.cluster.local

Name:      kubernetes
Address 1: 10.32.0.1 kubernetes.default.svc.cluster.local
```

Далее: [Smoke тест](13-smoke-test.md)
