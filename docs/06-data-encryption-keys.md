# Создание конфигурационного файла и ключа шифрования данных

Kubernetes хранит различные данные, включая состояние кластера, конфигурации приложений и секреты. Kubernetes поддерживает возможность [шифрования](https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data) данных кластера.

В этой главе вы создадите ключ и [конфигурацию шифрования,](https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/#understanding-the-encryption-at-rest-configuration) подходящую для шифрования секретов Kubernetes.

## Ключ шифрования

Сгенерируйте ключ шифрования:

```bash
ENCRYPTION_KEY=$(head -c 32 /dev/urandom | base64)
```

## Конфигурационный файл шифрования

Сгенерируйте конфигурационный файл шифрования `encryption-config.yaml`:

```bash
cat > encryption-config.yaml <<EOF
kind: EncryptionConfig
apiVersion: v1
resources:
  - resources:
      - secrets
    providers:
      - aescbc:
          keys:
            - name: key1
              secret: ${ENCRYPTION_KEY}
      - identity: {}
EOF
```

Скопируйте конфигурационный файл шифрования `encryption-config.yaml` на каждую master ноду:

```bash
scp encryption-config.yaml yc-user@${EXTERNAL_IP_MASTER_0}:~/
scp encryption-config.yaml yc-user@${EXTERNAL_IP_MASTER_1}:~/
scp encryption-config.yaml yc-user@${EXTERNAL_IP_MASTER_2}:~/
```

> Удостоверьтесь, что данные переменные окружения созданы. Данный шаг выполнялся в разделе [Добавление внешних IP-адресов инстансов в переменные окружения](https://gitlab.com/AlPervakov/yc-kubernetes-the-hart-way/-/blob/master/docs/03-compute-resources.md#добавление-внешних-ip-адресов-инстансов-в-переменные-окружения)

------

Далее: [Развертывание etcd кластера](07-bootstrapping-etcd.md)
