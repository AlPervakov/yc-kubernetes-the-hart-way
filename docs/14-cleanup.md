# Очистка Яндекс Облака

В этой главе вы удалите вычислительные ресурсы, созданные во время этого руководства.

## Инстансы

Удалите экземпляры ВМ:

```bash
for i in 0 1 2; do
  yc compute instance delete master-${i}
  yc compute instance delete worker-${i}
done
```

## Сеть

Удалите:

- таблицу маршрутизации `kubernetes-route`
- балансировщик нагрузки `kubernetes-load-balancer`
- целевую группу `kubernetes-target-pool`
- подсеть `kubernetes`
- VPC сеть `kubernetes-the-hard-way`

```bash
{
  yc vpc subnet update kubernetes --disassociate-route-table
  yc vpc route-table delete kubernetes-route
  yc load-balancer network-load-balancer delete kubernetes-load-balancer
  yc load-balancer target-group delete kubernetes-target-pool
  yc vpc subnet delete kubernetes
  yc vpc network delete kubernetes-the-hard-way 
}
```

Перейдите на страницу [консоли управления Яндекс Облака](https://console.cloud.yandex.ru/) и удалите статический IP-адрес:

1. Перейдите на страницу каталога, в котором нужно удалить адрес, и выберите сервис **Virtual Private Cloud**.
2. Выберите вкладку **IP-адреса**.
3. Удалите созданный IP-адрес.