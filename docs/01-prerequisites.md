# Предпосылки

## Яндекс Облако

В этом руководстве используется облачная платформа [Яндекс.Облако](https://cloud.yandex.ru/), чтобы поднять вычислительную инфраструктуру, необходимую для развертывания Kubernetes кластера с нуля.


## Интерфейс командной строки Яндекс Облако (YC CLI)

### Установите интерфейс командной строки (YC CLI)

Следуйте [документации](https://cloud.yandex.ru/docs/cli/operations/install-cli) Яндекса для установки и настройки `yc` интерфейса командной строки. 

Убедитесь, что YC CLI имеет версию 0.55.0 или выше:

```bash
yc version
```

### Установка зоны доступности по умолчанию

В данном руководстве предполагается, что зона доступности виртуальных машин по умолчанию настроены.

Если вы впервые используете инструмент командной строки  `yc` , то `init` это самый простой способ произвести [первоначальную настройку](https://cloud.yandex.ru/docs/cli/operations/authentication/user) инструмента:

```bash
yc init
```

Выполните все необходимые действия, которые будут предложены в терминале:

1. По запросу команды введите свой [OAuth токен](https://oauth.yandex.ru/authorize?response_type=token&client_id=1a6990aa636648e9b2ef855fa7bec2fb):

   ```bash
   Please go to https://oauth.yandex.ru/authorize?response_type=token&client_id=1a6990aa636648e9b2ef855fa7bec2fb
    in order to obtain OAuth token.
   
   Please enter OAuth token: AaAaBbBbCcCcDdDdEeEeFfFfGgGg
   ```

2. Выберите одно из предложенных облаков, в которых у вас есть права доступа:

   ```bash
   Please select cloud to use:
    [1] cloud1 (id = aoe2bmdcvatao4frg22b)
    [2] cloud2 (id = dcvatao4faoe2bmrg22b)
   Please enter your numeric choice: 1
   ```

   > Если вам доступно только одно облако, оно будет выбрано автоматически.

3. Выберите каталог по умолчанию:

   ```bash
   Please choose a folder to use:
    [1] folder1 (id = cvatao4faoe2bmdrg22b)
    [2] folder2 (id = tao4faoe2cvabmdrg22b)
    [3] Create a new folder
   Please enter your numeric choice: 1
   ```

4. Выберите зону доступности по умолчанию для сервиса Yandex Compute Cloud:

   ```
   Do you want to configure a default Yandex Compute Cloud availability zone? [Y/n] Y
   Which zone do you want to use as a profile default?
    [1] ru-central1-a
    [2] ru-central1-b
    [3] ru-central1-c
    [4] Don't set default zone
   Please enter your numeric choice: 1
   ```

   > Далее в руководстве будет использоваться зона доступности `ru-central1-a`

5. Проверьте настройки вашего профиля CLI:

   ```
   $ yc config list
   ```

   

## Параллельный запуск команд с использованием tmux

[tmux](https://github.com/tmux/tmux/wiki) можно использовать для одновременного запуска команд на нескольких виртуальных машинах. В данном руководстве есть моменты, когда необходимо ввести одни и те же команды на разных машинах, в этих случаях рассмотрите возможность использования tmux для разделения окна терминала на несколько рабочих областей с возможностью синхронного ввода команд. Это ускорит процесс настройки машин.

> Использование tmux не является обязательным и не требуется для завершения этого руководства. Вы можете вводить одни и те же команды на разные виртуальные машины вручную. Ввод таких команд начнется с главы 7.

![tmux screenshot](https://raw.githubusercontent.com/kelseyhightower/kubernetes-the-hard-way/master/docs/images/tmux-screenshot.png)

> По умолчанию для активации сочетания клавиш нужно нажать `сtrl + b`, отпустить, а затем нажать нужную клавишу. Чтобы разделить окно на несколько панелей (рабочих областей) горизонтально нажмите `ctrl + b`, а затем `"`  или `%` для вертикального разделения. Переход между рабочими панелями осуществляется с помощью `сtrl+b` и последующего нажатия стрелки на клавиатуре. Чтобы закрыть панель нажмите `сtrl + b` и `x` (находясь в панели, которую нужно закрыть). Для включения синхронизации ввода нажмите `ctrl + b`, затем `shift + :`, что позволит перейти в режим ввода, и введите фразу `set synchronize-panes on`, а для отключения введите `set synchronize-panes off`.

------

Далее: [Установка клиентских инструменов](02-client-tools.md) 

