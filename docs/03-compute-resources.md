# Подготовка виртуальных машин

Для Kubernetes требуется набор машин для размещения [управляющих компонентов Kubernetes и рабочих узлов](https://kubernetes.io/docs/concepts/), на которых в конечном итоге запускаются контейнеры. В этой главе вы подготовите инстансы, необходимые для запуска безопасного и высокодоступного кластера Kubernetes в одной [зоне доступности](https://cloud.yandex.ru/docs/overview/concepts/geo-scope).

> Убедитесь, что вы выставили зону доступности по умолчанию, как описано в главе [Предпосылки](https://gitlab.com/AlPervakov/yc-kubernetes-the-hart-way/-/blob/master/docs/01-prerequisites.md#установка-зоны-доступности-по-умолчанию).

## Сеть

[Сетевая модель Kubernetes](https://kubernetes.io/docs/concepts/cluster-administration/networking/#kubernetes-model) предполагает плоскую сеть, в которой контейнеры и ноды (узлы) могут связываться друг с другом. В тех случаях, когда это нежелательно, [сетевые политики](https://kubernetes.io/docs/concepts/services-networking/network-policies/) могут ограничивать способы взаимодействия групп контейнеров друг с другом и с внешними конечными точками сети.

> Настройка сетевых политик выходит за рамки данного руководства.

### Виртуальная частная облачная сеть (VPC сеть)

В этом разделе будет настроена выделенная облачная сеть [Virtual Private Cloud](https://cloud.yandex.ru/docs/vpc/) (VPC) для размещения кластера Kubernetes.

Создайте `kubernetes-the-hard-way` [пользовательскую VPC сеть](https://cloud.yandex.ru/docs/vpc/operations/network-create):

```bash
yc vpc network create --name kubernetes-the-hard-way \
  --description "Network for Kubernetes cluster" \
  --folder-id ID_ВАШЕГО_КАТАЛОГА
```

> `folder-id` можно узнать введя команду `yc config profile get default`. Чтобы понять, информацию какого профиля вам нужно получить, используйте команду `yc config profile list`. Для данного руководства это профиль `default`.

Для [подсети](https://cloud.yandex.ru/docs/vpc/operations/subnet-create) должен быть предусмотрен диапазон IP-адресов, достаточно большой для назначения частного IP-адреса каждому узлу в кластере Kubernetes.

Создайте `kubernetes` подсеть в `kubernetes-the-hard-way` VPC сети:

```bash
yc vpc subnet create --name kubernetes \
  --description "My kubernetes subnet" \
  --folder-id ID_ВАШЕГО_КАТАЛОГА \
  --network-id ID_ВАШЕЙ_СЕТИ \
  --zone ru-central1-a \
  --range 10.240.0.0/24
```

> Узнать **network-id** `kubernetes-the-hard-way` можно используя команду `yc vpc network list`. Также id сети будет выведен в терминал сразу после создания сети.

> Диапазон 10.240.0.0/24 IP-адресов может содержать до 254 вычислительных экземпляров.

### Публичный IP-адрес Kubernetes

[Зарезервируйте статический публичный IP-адрес](https://cloud.yandex.ru/docs/vpc/operations/get-static-ip), который будет подключен к внешнему балансировщику нагрузки на Kubernetes API серверах:


1. Перейдите на страницу [консоли управления Яндекс Облака](https://console.cloud.yandex.ru/).
2. Перейдите на страницу каталога, в котором нужно зарезервировать адрес, и выберите сервис **Virtual Private Cloud**.
3. Выберите вкладку **IP-адреса**.
4. Нажмите кнопку **Зарезервировать адрес**.
5. В открывшемся окне выберите зону доступности, в которой нужно зарезервировать адрес.
6. Нажмите кнопку **Зарезервировать**.             


Убедитесь, что IP-адрес был создан в зоне доступности указанной по умолчанию (для данного руководства это `ru-central1-a`).

## Экземпляры виртуальных машин

Экземпляры виртуальных машин в этой главе будут подготовлены с использованием образа [Ubuntu Server](https://www.ubuntu.com/server) 18.04, который хорошо поддерживает [containerd container runtime](https://github.com/containerd/containerd). Каждой ВМ будет предоставлен фиксированный частный IP-адрес, чтобы упростить процесс развертывания Kubernetes.

### Kubernetes Masters ноды

Подготовьте пару ключей (открытый и закрытый) для SSH-доступа на виртуальную машину. 

Создайте три ВМ, в которых будут размещены управляющие компоненты Kubernetes:

```bash
for i in 0 1 2; do
  yc compute instance create \
    --name master-${i} \
    --async \
    --memory=4 \
    --create-boot-disk image-folder-id=standard-images,image-family=ubuntu-1804,size=20GB \
    --network-interface subnet-name=kubernetes,nat-ip-version=ipv4,address=10.240.0.1${i} \
    --metadata serial-port-enable=1 \
    --ssh-key ~/.ssh/id_rsa.pub
done
```

### Kubernetes Workers ноды

Каждому экземпляру worker ноды требуется выделение подсети пода из диапазона CIDR Kubernetes  кластера. Выделение подсети пода будет использоваться для настройки сети контейнера в дальнейших упражнениях. Метаданные инстанса `pod-cidr` будут использоваться, чтобы распределить сеть пода для ВМ во время выполнения.

> CIDR диапозон Kubernetes кластера определяется флагом `--cluster-cidr` диспетчером контроллеров. В этом руководстве CIDR диапазон кластера будет установлен на `10.200.0.0/16`, который поддерживает 254 подсети.

Создайте три ВМ, которые в дальнейшем будут являться worker нодами Kubernetes:

```bash
for i in 0 1 2; do
  yc compute instance create \
    --name worker-${i} \
    --hostname worker-${i} \
    --async \
    --memory=4 \
    --create-boot-disk image-folder-id=standard-images,image-family=ubuntu-1804,size=20GB \
    --network-interface subnet-name=kubernetes,nat-ip-version=ipv4,address=10.240.0.2${i} \
    --metadata serial-port-enable=1,pod-cidr=10.200.${i}.0/24 \
    --ssh-key ~/.ssh/id_rsa.pub
done
```

### Верификация

Выведите список ВМ в зоне доступности по умолчанию:

```bash
yc compute instances list
```

> вывод

```bash
+----------+----------+---------------+---------+----------------+-------------+
|    ID    |   NAME   |    ZONE ID    | STATUS  |  EXTERNAL IP   | INTERNAL IP |
+----------+----------+---------------+---------+----------------+-------------+
| xxxxxxxx | master-2 | ru-central1-a | RUNNING | XX.XXX.XXX.XXX | 10.240.0.12 |
| xxxxxxxx | worker-1 | ru-central1-a | RUNNING | XXX.XXX.XX.XX  | 10.240.0.21 |
| xxxxxxxx | worker-0 | ru-central1-a | RUNNING | XX.XXX.XXX.XXX | 10.240.0.20 |
| xxxxxxxx | master-0 | ru-central1-a | RUNNING | XX.XXX.XXX.XX  | 10.240.0.10 |
| xxxxxxxx | worker-2 | ru-central1-a | RUNNING | XXX.XXX.XX.XXX | 10.240.0.22 |
| xxxxxxxx | master-1 | ru-central1-a | RUNNING | XX.XXX.XXX.XXX | 10.240.0.11 |
+----------+----------+---------------+---------+----------------+-------------+
```

## Подключение к инстансам по SSH

Ранее мы создали пару SSH ключей. Теперь проверим [подключение к созданным инстансам](https://cloud.yandex.ru/docs/compute/operations/vm-connect/ssh):

```bash
ssh -i ~/.ssh/id_rsa yc-user@EXTERNAL_IP
```
> где `EXTERNAL_IP` - внешний IP-адрес вашего инстанса

```bash
Welcome to Ubuntu 18.04.3 LTS (GNU/Linux 4.15.0-1042-gcp x86_64)
...
```

Введите `exit` в командной строке, чтобы выйти из инстанса:

```bash
yc-user@aAaBbBbCcCcDdDdEeEeF:~$ exit
```
> вывод

```bash
logout
Connection to XX.XXX.XXX.XXX closed.
```

## Добавление внешних IP-адресов инстансов в переменные окружения

Так как документация Яндекс Облака предлагает нам подключаться к инстансам используя `ssh` клиент, добавим внешние IP-адреса наших ВМ для дальнейшего упрощения процесса передачи конфигурационных фалов и сертификатов c помощью утилиты `scp`: 

- для worker нод:


```bash
for instance in 0 1 2; do
  export EXTERNAL_IP_WORKER_${instance}=$(yc compute instance get worker-${instance} \
  | grep -A1 one_to_one_nat: \
  | grep -oE '\b[0-9]{1,3}(\.[0-9]{1,3}){3}\b')
done
```

- для master нод:

```bash
for instance in 0 1 2; do
  export EXTERNAL_IP_MASTER_${instance}=$(yc compute instance get master-${instance} \
  | grep -A1 one_to_one_nat: \
  | grep -oE '\b[0-9]{1,3}(\.[0-9]{1,3}){3}\b')
done
```

> Не забудьте, что данные локальные переменные окружения действительны только для текущей терминальной сессии!

------

Далее: [Подготовка CA и генерация TLS сертификатов](04-certificate-authority.md)
