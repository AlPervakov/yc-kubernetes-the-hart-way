# Smoke тест

В этой главе вы выполните ряд задач, чтобы убедиться, что ваш кластер Kubernetes работает правильно.

## Шифрование данных

В этом разделе вы проверите возможность [шифрования данных](https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/#verifying-that-data-is-encrypted).

Создайте общий секрет:

```bash
kubectl create secret generic kubernetes-the-hard-way \
  --from-literal="mykey=mydata"
```

Выведите hexdump `kubernetes-the-hard-way` секрета, хранящегося в etcd:

подключитесь к master ноде по ssh и выполните команду

```bash
sudo ETCDCTL_API=3 etcdctl get \
  --endpoints=https://127.0.0.1:2379 \
  --cacert=/etc/etcd/ca.pem \
  --cert=/etc/etcd/kubernetes.pem \
  --key=/etc/etcd/kubernetes-key.pem\
  /registry/secrets/default/kubernetes-the-hard-way | hexdump -C
```

> вывод

```bash
00000000  2f 72 65 67 69 73 74 72  79 2f 73 65 63 72 65 74  |/registry/secret|
00000010  73 2f 64 65 66 61 75 6c  74 2f 6b 75 62 65 72 6e  |s/default/kubern|
00000020  65 74 65 73 2d 74 68 65  2d 68 61 72 64 2d 77 61  |etes-the-hard-wa|
00000030  79 0a 6b 38 73 3a 65 6e  63 3a 61 65 73 63 62 63  |y.k8s:enc:aescbc|
00000040  3a 76 31 3a 6b 65 79 31  3a 26 d2 73 2e 11 73 59  |:v1:key1:&.s..sY|
00000050  46 d8 15 53 69 66 4b 11  fe 0c 20 1b ef fa d1 20  |F..SifK... .... |
00000060  f2 e3 90 d8 a2 3f 34 8d  a1 dc 3f 3b a2 77 64 df  |.....?4...?;.wd.|
00000070  f3 86 b9 bc c9 7e e6 64  78 20 35 c4 34 7f 81 3d  |.....~.dx 5.4..=|
00000080  ec 1f df b4 70 ae 11 57  88 63 3e 90 c5 37 5b 45  |....p..W.c>..7[E|
00000090  66 28 ef c6 6b 50 90 d6  eb 95 b6 4a a3 27 84 ae  |f(..kP.....J.'..|
000000a0  32 fe 42 7d ee 58 2b 83  c1 d2 92 f1 b8 16 74 54  |2.B}.X+.......tT|
000000b0  2f 7b 6d 51 0b 31 ca 99  2e fb 54 29 50 26 43 95  |/{mQ.1....T)P&C.|
000000c0  15 bc 7f 52 35 4a 0b c8  1d bf 9a 3f 7a c0 4d a8  |...R5J.....?z.M.|
000000d0  d5 ba fe 75 41 0b 0a 5b  d1 cb 33 c2 1a d1 bb c3  |...uA..[..3.....|
000000e0  44 52 ba d5 44 59 ac 11  4f 0a                    |DR..DY..O.|
```

К etcd ключу должен быть добавлен префикс `k8s:enc:aescbc:v1:key1`, который указывает, что `aescbc` провайдер использовался для шифрования данных с помощью ключа шифрования `key1`.

## Развертывание

В этом разделе вы проверите возможность создания и управления [развертывания](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/).

Создайте развертывание для веб-сервера [nginx](https://nginx.org/en/):

```bash
kubectl create deployment nginx --image=nginx
```

Выведите под, созданный при развертывании `nginx`:

```bash
kubectl get pods -l app=nginx
```

> вывод

```bash
NAME                     READY   STATUS    RESTARTS   AGE
nginx-554b9c67f9-q6dhp   1/1     Running   0          17s
```

### Перенаправление порта

В этом разделе вы проверите возможность удаленного доступа к приложениям с помощью [переадресации портов](https://kubernetes.io/docs/tasks/access-application-cluster/port-forward-access-application-cluster/).

Получить полное имя `nginx` пода:

```bash
POD_NAME=$(kubectl get pods -l app=nginx -o jsonpath="{.items[0].metadata.name}")
```

Перенаправьте порт `8080` на локальном компьютере к порту `80`  в поде `nginx`:

```bash
kubectl port-forward $POD_NAME 8080:80
```

> вывод

```bash
Forwarding from 127.0.0.1:8080 -> 80
Forwarding from [::1]:8080 -> 80
```

В новом терминале сделайте HTTP-запрос, используя адрес пересылки:

```bash
curl --head http://127.0.0.1:8080
```

> вывод

```bash
HTTP/1.1 200 OK
Server: nginx/1.17.10
Date: Thu, 30 Apr 2020 18:55:54 GMT
Content-Type: text/html
Content-Length: 612
Last-Modified: Tue, 14 Apr 2020 14:19:26 GMT
Connection: keep-alive
ETag: "5e95c66e-264"
Accept-Ranges: bytes
```

Вернитесь к предыдущему терминалу и остановите переадресацию порта на под `nginx`:

```bash
Forwarding from 127.0.0.1:8080 -> 80
Forwarding from [::1]:8080 -> 80
Handling connection for 8080
^C
```

### Логи

В этом разделе вы проверите возможность [получения логов контейнера](https://kubernetes.io/docs/concepts/cluster-administration/logging/).

Выведите логи пода `nginx`:

```bash
kubectl logs $POD_NAME
```

> вывод

```bash
127.0.0.1 - - [30/Apr/2020:18:55:54 +0000] "HEAD / HTTP/1.1" 200 0 "-" "curl/7.58.0" "-"
```

### Exec

В этом разделе вы проверите возможность [выполнения команд в контейнере](https://kubernetes.io/docs/tasks/debug-application-cluster/get-shell-running-container/#running-individual-commands-in-a-container).

Выведите версию nginx, выполнив `nginx -v` команду в `nginx` контейнере:

```bash
kubectl exec -ti $POD_NAME -- nginx -v
```

> вывод

```bash
nginx version: nginx/1.17.10
```

## Сервисы

В этом разделе вы проверите возможность выставлять приложение наружу используя [Service](https://kubernetes.io/docs/concepts/services-networking/service/).

Выставьте `nginx` развертывание используя [NodePort](https://kubernetes.io/docs/concepts/services-networking/service/#type-nodeport) сервис:

```ba
kubectl expose deployment nginx --port 80 --type NodePort
```

> Нельзя использовать тип службы LoadBalancer, поскольку в вашем кластере не настроена [интеграция с облачным провайдером](https://kubernetes.io/docs/getting-started-guides/scratch/#cloud-provider). Настройка интеграции с облачным провайдером выходит за рамки данного руководства.

Получить порт узла, назначенный `nginx` сервису:

```bash
NODE_PORT=$(kubectl get svc nginx \
  --output=jsonpath='{range .spec.ports[0]}{.nodePort}')
```

Получить внешний IP-адрес worker ноды:

```bash
EXTERNAL_IP=$(yc compute instance get worker-0 \
  | grep -A1 one_to_one_nat: \
  | grep -oE '\b[0-9]{1,3}(\.[0-9]{1,3}){3}\b')
```

Сделайте HTTP-запрос, используя внешний IP-адрес и порт ноды `nginx`:

```
curl -I http://${EXTERNAL_IP}:${NODE_PORT}
```

> вывод

```
HTTP/1.1 200 OK
Server: nginx/1.17.10
Date: Thu, 30 Apr 2020 18:58:06 GMT
Content-Type: text/html
Content-Length: 612
Last-Modified: Tue, 14 Apr 2020 14:19:26 GMT
Connection: keep-alive
ETag: "5e95c66e-264"
Accept-Ranges: bytes
```

------

Далее: [Очистка Яндекс Облака](14-cleanup.md)
