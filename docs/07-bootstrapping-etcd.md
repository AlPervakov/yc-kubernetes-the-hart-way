# Развертывание etcd кластера

Компоненты Kubernetes не имеют состояния. Основным хранилищем всех данных кластера в Kubernetes выступает [etcd](https://github.com/etcd-io/etcd) (распределённое и высоконадёжное хранилище данных в формате “ключ-значение”).  В этой главе вы загрузите кластер etcd с тремя узлами и настроите его для обеспечения высокой доступности и безопасного удаленного доступа.

## Предпосылки

Команды в этой лабораторной работе , должны выполняться на каждом экземпляре master ноды: `master-0`, `master-1` и `master-2`. Войдите в каждый экземпляр мастера с помощью ssh. Пример:

```bash
ssh -i ~/.ssh/id_rsa yc-user@EXTERNAL_IP
```

> где `EXTERNAL_IP` - внешний IP-адрес вашей master ноды

### Параллельный запуск команд с помощью tmux

[tmux](https://github.com/tmux/tmux/wiki) можно использовать для одновременного запуска команд на нескольких ВМ. Смотрите раздел [Параллельный запуск команд с использованием tmux](https://gitlab.com/AlPervakov/yc-kubernetes-the-hart-way/-/blob/master/docs/01-prerequisites.md#параллельный-запуск-команд-с-использованием-tmux).

## Развертывание компонентов etcd кластера

### Загрузите и установите бинарные файлы etcd

Загрузите бинарные файлы с официального релиза etcd из проекта [etcd](https://github.com/etcd-io/etcd) на GitHub:

```bash
wget -q --show-progress --https-only --timestamping \
  "https://github.com/etcd-io/etcd/releases/download/v3.4.0/etcd-v3.4.0-linux-amd64.tar.gz"
```

Распакуйте и установите `etcd` сервер и утилиту командной строки `etcdctl`:

```bash
{
  tar -xvf etcd-v3.4.0-linux-amd64.tar.gz
  sudo mv etcd-v3.4.0-linux-amd64/etcd* /usr/local/bin/
}
```

### Сконфигурируйте etcd сервер

```bash
{
  sudo mkdir -p /etc/etcd /var/lib/etcd
  sudo cp ca.pem kubernetes-key.pem kubernetes.pem /etc/etcd/
}
```

Внутренний IP-адрес ВМ будет использоваться для обслуживания клиентских запросов и обмена данными с узлами кластера etcd. [Получите внутренний IP-адрес](https://cloud.yandex.ru/docs/compute/operations/vm-info/get-info#gce-metadata) для текущей ВМ:

```bash
INTERNAL_IP=$(curl -s -H "Metadata-Flavor: Google" \
  169.254.169.254/computeMetadata/v1/instance/network-interfaces/0/ip)
```

Каждый компонент etcd должен иметь уникальное имя в кластере etcd. Установите имя etcd в соответствии с именем текущей ВМ:

```bash
ETCD_NAME=$(curl -s -H "Metadata-Flavor: Google" \
  169.254.169.254/computeMetadata/v1/instance/name)
```

Создайте systemd файл `etcd.service`:

```bash
cat <<EOF | sudo tee /etc/systemd/system/etcd.service
[Unit]
Description=etcd
Documentation=https://github.com/coreos

[Service]
Type=notify
ExecStart=/usr/local/bin/etcd \\
  --name ${ETCD_NAME} \\
  --cert-file=/etc/etcd/kubernetes.pem \\
  --key-file=/etc/etcd/kubernetes-key.pem \\
  --peer-cert-file=/etc/etcd/kubernetes.pem \\
  --peer-key-file=/etc/etcd/kubernetes-key.pem \\
  --trusted-ca-file=/etc/etcd/ca.pem \\
  --peer-trusted-ca-file=/etc/etcd/ca.pem \\
  --peer-client-cert-auth \\
  --client-cert-auth \\
  --initial-advertise-peer-urls https://${INTERNAL_IP}:2380 \\
  --listen-peer-urls https://${INTERNAL_IP}:2380 \\
  --listen-client-urls https://${INTERNAL_IP}:2379,https://127.0.0.1:2379 \\
  --advertise-client-urls https://${INTERNAL_IP}:2379 \\
  --initial-cluster-token etcd-cluster-0 \\
  --initial-cluster master-0=https://10.240.0.10:2380,master-1=https://10.240.0.11:2380,master-2=https://10.240.0.12:2380 \\
  --initial-cluster-state new \\
  --data-dir=/var/lib/etcd
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF
```

### Запуск etcd сервера

```bash
{
  sudo systemctl daemon-reload
  sudo systemctl enable etcd
  sudo systemctl start etcd
}
```

> Не забудьте, что указанные выше команды должны быть выполнены на каждом узле: `master-0`, `master-1` и `master-2`.

## Верификация

Список компонентов etcd кластера:

```bash
sudo ETCDCTL_API=3 etcdctl member list \
  --endpoints=https://127.0.0.1:2379 \
  --cacert=/etc/etcd/ca.pem \
  --cert=/etc/etcd/kubernetes.pem \
  --key=/etc/etcd/kubernetes-key.pem
```

> вывод

```bash
3a57933972cb5131, started, master-2, https://10.240.0.12:2380, https://10.240.0.12:2379
f98dc20bce6225a0, started, master-0, https://10.240.0.10:2380, https://10.240.0.10:2379
ffed16798470cab5, started, master-1, https://10.240.0.11:2380, https://10.240.0.11:2379
```

Далее: [Развертывание управляющих компонентов Kubernetes](08-bootstrapping-kubernetes-controllers.md)
